% Plots for task A.
% /Rostyslav Skrypnyk.

figure(2)
% Plot the total vertical reaction force vs the displacement:
load('penalty1e5.mat')
plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_hist_w(dof_prescr_vert_w,:),1),'*-')
hold on

load('penalty1e6.mat')
plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_hist_w(dof_prescr_vert_w,:),1),'r^-')
load('penalty1e7.mat')

plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_hist_w(dof_prescr_vert_w,:),1),'ks-')
hold off
xlabel('Normalized vertical displacement, [unitless]', 'interpreter','latex','fontsize', 18)
ylabel('Total vertical reaction force, [N]', 'interpreter','latex','fontsize',18)
legend({'$\epsilon_N = 1e5$ N/mm','$\epsilon_N = 1e6$ N/mm',...
        '$\epsilon_N = 1e7$ N/mm'},'interpreter','latex',...
        'location', 'best')
h = gca;
h.FontSize = 14;
set(h,'TickLabelInterpreter', 'latex');
% print -depsc -r600 ./doc/img/force_displ % Create file for LaTeX.

figure(3)
% Plot von Mises stress:
fill(mesh_w.Ex',mesh_w.Ey',stress_Mises_hist_w(:,end)');
hold on
fill(mesh_r.Ex',mesh_r.Ey',stress_Mises_hist_r(:,end)');
c = colorbar; 
axis equal;
title('von Mises stress, [MPa]','interpreter','latex','fontsize',18)
xlabel('$x$, [mm]', 'interpreter','latex','fontsize',18)
ylabel('$y$, [mm]', 'interpreter','latex','fontsize', 18)
h = gca;
h.FontSize = 14;
set(h,'TickLabelInterpreter', 'latex');
c.TickLabelInterpreter = 'latex';
% print -depsc -r600 ./doc/img/von_mises % Create file for LaTeX.

figure(4)
% Plot the total vertical reaction force vs the displacement:
load('penalty1e5.mat')
mpe5 = max_penetration;
load('penalty1e6.mat')
mpe6 = max_penetration;
load('penalty1e7.mat')
mpe7 = max_penetration;
plot([1e5, 1e6, 1e7], ...
     abs([mpe5, mpe6, mpe7]), '*-')
xlabel('Penalty parameter $\epsilon_N$, [N/mm]', 'interpreter','latex','fontsize',18)
ylabel('Maximum interpenetration, [mm]', 'interpreter','latex','fontsize', 18)
h = gca;
h.FontSize = 14;
set(h,'TickLabelInterpreter', 'latex');
% print -depsc -r600 ./doc/img/penalty_penetration % Create file for LaTeX.