function [du_w, rhs_global_w, stress_Mises_array_w, ...
          du_r, rhs_global_r, stress_Mises_array_r] = findEquilibrium(...
                Edof_w, Ex_w, Ey_w, ...
                Edof_r, Ex_r, Ey_r, ...
                u_w, du_w, n_el_w, n_dof_w, dof_free_w, ...
                contact_nodes_coords_w, contact_dofs_w, ...
                active_nodes_w, ...
                u_r, du_r, n_el_r, n_dof_r, dof_free_r, ...
                contact_nodes_coords_r, contact_dofs_r, ...
                xi_l_array, xi_r_array, normal_array, ...
                mat_par, max_iter, err_tol, gap_tol)
%Function to represent FE equilibrium iteration.
%   Written to work for Penalty method.
%
% Rostyslav Skrypnyk.

active_node_numbers_w = find(active_nodes_w);

% Create new arrays for the contact system, since 
% the system of equations cannot be decoupled:
Edof_r_renumbered = [n_el_w + Edof_r(:,1), n_dof_w + Edof_r(:,2:end)];
Edof = [Edof_w; Edof_r_renumbered]; % Convention: wheel 1st, rail 2nd.
Ex = [Ex_w; Ex_r];
Ey = [Ey_w; Ey_r];
u = [u_w; u_r]; 
du = [du_w; du_r];
n_el = n_el_w + n_el_r;
n_dof = n_dof_w + n_dof_r;
dof_free_r_renumbered = n_dof_w + dof_free_r;
contact_dofs_r_renumbered = n_dof_w + contact_dofs_r;
dof_free = [dof_free_w; dof_free_r_renumbered];

stress_Mises_array = zeros(n_el,1); % To store von Mises stress in
% the elements.

converge = false; % Assume it does not conferge.

for iter=1:max_iter
    u_el_array = extract(Edof,u);
    % Extract nodal increments of the displacement 
    % for each element:
    du_el_array = extract(Edof,du);
    
    K_global = zeros(n_dof); % Global stiffness matrix.
    rhs_global = zeros(n_dof,1); % Global right hand side vector ==
    % internal forces - external forces.
    
    %% Assemble stiffness matrix and RHS vector
    for el=1:n_el
        [K_el, f_el, stresses_el] = elementRoutine(Ex(el,:), Ey(el,:),...
            mat_par, u_el_array(el,:)', du_el_array(el,:)');        
        
        K_global(Edof(el,2:end),Edof(el,2:end)) = ...
            K_global(Edof(el,2:end),Edof(el,2:end)) + K_el;
        rhs_global(Edof(el,2:end)) = ...
            rhs_global(Edof(el,2:end)) + f_el;
        
        % For plotting:
        % Compute von Mises stress in the element:
        stresses_dev_el = stresses_el;
        stresses_dev_el(1:3) = stresses_el(1:3) - ...
            ones(3,1)*sum(stresses_el(1:3))/3.0;
        stress_Mises = sqrt(1.5* (stresses_dev_el'*stresses_dev_el) );
        stress_Mises_array(el) = stress_Mises;
    end % Loop over elements.    
    %% Add contribution to the stiffness matrix and RHS vector from the contact nodes:
    contact_nodes_coords_w_new = contact_nodes_coords_w + ...
                                 u(contact_dofs_w) + ...
                                 du(contact_dofs_w);
    contact_nodes_coords_r_new = contact_nodes_coords_r +...
                                 u(contact_dofs_r_renumbered) +...
                                 du(contact_dofs_r_renumbered);
    % Get active slave nodes:
    [~, ~, ~, gap_func_array, ~] = contactSearch(contact_nodes_coords_w_new, ...
                                                 contact_nodes_coords_r_new, ...
                                                 gap_tol);
    if ~isempty(active_node_numbers_w)
        % For slave nodes (wheel):
        for node_w = active_node_numbers_w' % input must be a row vector!
            % Find the closest master node together with left and right nodes to it:
            [~,~,~,~,active_nodes_r] = contactSearch(contact_nodes_coords_w_new(node_w,:), ...
                                                     contact_nodes_coords_r_new, ...
                                                     gap_tol);
            node_r = find(active_nodes_r);
            [~, i_rail_sorted] = sortrows(contact_nodes_coords_r,1);
            [~,node_r_sorted] = min( abs(i_rail_sorted - node_r) );

            node_r_left = i_rail_sorted(node_r_sorted-1);
            node_r_right = i_rail_sorted(node_r_sorted+1);
            
            % Get nodal DOFs:
            node_active_dofs_w = contact_dofs_w(node_w,:);
            node_active_dofs_r = contact_dofs_r_renumbered(node_r,:);
            node_active_dofs_r_left = contact_dofs_r_renumbered(node_r_left,:);
            node_active_dofs_r_right = contact_dofs_r_renumbered(node_r_right,:);
            
            F_n = mat_par.penalty * gap_func_array(node_w);
            
            % Make simplification that normal, xi_left and xi_right are
            % constant during equilibrium iteration:
            N_vector = normal_array(node_w,:);
            xi_left = xi_l_array(node_w);
            xi_right = xi_r_array(node_w);
            
            % Derivatives for wheel:
            dGapFunc_dx_slave = N_vector;
            % and rail:
            dGapFunc_dx_closest = (xi_left + xi_right - 1) * N_vector;
            dGapFunc_dx_left = - xi_left * N_vector;
            dGapFunc_dx_right = - xi_right * N_vector;
            
            % Add contributions to the stiffness matrix:
            % RHS(slave_dof):
            K_global(node_active_dofs_w, node_active_dofs_w) = ...
                K_global(node_active_dofs_w, node_active_dofs_w) + ...
                mat_par.penalty*( dGapFunc_dx_slave' * dGapFunc_dx_slave );
            %
            K_global(node_active_dofs_w, node_active_dofs_r) = ...
                K_global(node_active_dofs_w, node_active_dofs_r) + ...
                mat_par.penalty*( dGapFunc_dx_slave' * dGapFunc_dx_closest );
            %
            K_global(node_active_dofs_w, node_active_dofs_r_left) = ...
                K_global(node_active_dofs_w, node_active_dofs_r_left) + ...
                mat_par.penalty*( dGapFunc_dx_slave' * dGapFunc_dx_left );
            %
            K_global(node_active_dofs_w, node_active_dofs_r_right) = ...
                K_global(node_active_dofs_w, node_active_dofs_r_right) + ...
                mat_par.penalty*( dGapFunc_dx_slave' * dGapFunc_dx_right );
            % RHS(closest_dof):
            K_global(node_active_dofs_r, node_active_dofs_w) = ...
                K_global(node_active_dofs_r, node_active_dofs_w) + ...
                mat_par.penalty*( dGapFunc_dx_closest' * dGapFunc_dx_slave );
            %
            K_global(node_active_dofs_r, node_active_dofs_r) = ...
                K_global(node_active_dofs_r, node_active_dofs_r) + ...
                mat_par.penalty*( dGapFunc_dx_closest' * dGapFunc_dx_closest );
            %
            K_global(node_active_dofs_r, node_active_dofs_r_left) = ...
                K_global(node_active_dofs_r, node_active_dofs_r_left) + ...
                mat_par.penalty*( dGapFunc_dx_closest' * dGapFunc_dx_left );
            %
            K_global(node_active_dofs_r, node_active_dofs_r_right) = ...
                K_global(node_active_dofs_r, node_active_dofs_r_right) + ...
                mat_par.penalty*( dGapFunc_dx_closest' * dGapFunc_dx_right );
            % RHS(left_from_closest_dof):
            K_global(node_active_dofs_r_left, node_active_dofs_w) = ...
                K_global(node_active_dofs_r_left, node_active_dofs_w) + ...
                mat_par.penalty*( dGapFunc_dx_left' * dGapFunc_dx_slave );
            %
            K_global(node_active_dofs_r_left, node_active_dofs_r) = ...
                K_global(node_active_dofs_r_left, node_active_dofs_r) + ...
                mat_par.penalty*( dGapFunc_dx_left' * dGapFunc_dx_closest );
            %
            K_global(node_active_dofs_r_left, node_active_dofs_r_left) = ...
                K_global(node_active_dofs_r_left, node_active_dofs_r_left) + ...
                mat_par.penalty*( dGapFunc_dx_left' * dGapFunc_dx_left );
            %
            K_global(node_active_dofs_r_left, node_active_dofs_r_right) = ...
                K_global(node_active_dofs_r_left, node_active_dofs_r_right) + ...
                mat_par.penalty*( dGapFunc_dx_left' * dGapFunc_dx_right );
            % RHS(right_from_closest_dof):
            K_global(node_active_dofs_r_right, node_active_dofs_w) = ...
                K_global(node_active_dofs_r_right, node_active_dofs_w) + ...
                mat_par.penalty*( dGapFunc_dx_right' * dGapFunc_dx_slave );
            %
            K_global(node_active_dofs_r_right, node_active_dofs_r) = ...
                K_global(node_active_dofs_r_right, node_active_dofs_r) + ...
                mat_par.penalty*( dGapFunc_dx_right' * dGapFunc_dx_closest );
            %
            K_global(node_active_dofs_r_right, node_active_dofs_r_left) = ...
                K_global(node_active_dofs_r_right, node_active_dofs_r_left) + ...
                mat_par.penalty*( dGapFunc_dx_right' * dGapFunc_dx_left );
            %
            K_global(node_active_dofs_r_right, node_active_dofs_r_right) = ...
                K_global(node_active_dofs_r_right, node_active_dofs_r_right) + ...
                mat_par.penalty*( dGapFunc_dx_right' * dGapFunc_dx_right );          
   
            % Finally, add contribution to the residuals of the wheel:
            rhs_global(node_active_dofs_w) = ...
                rhs_global(node_active_dofs_w) + dGapFunc_dx_slave' * F_n;
            % and the rail closest:
            rhs_global(node_active_dofs_r) = ...
                rhs_global(node_active_dofs_r) + dGapFunc_dx_closest' * F_n;
            % rail left from closest:
            rhs_global(node_active_dofs_r_left) = ...
                rhs_global(node_active_dofs_r_left) + dGapFunc_dx_left' * F_n;
            % rail right from closest:
            rhs_global(node_active_dofs_r_right) = ...
                rhs_global(node_active_dofs_r_right) + dGapFunc_dx_right' * F_n;
        end % loop over active nodes.
    end % if there are active nodes.
    %% Solve the system and update unknowns:
    delta_du =  - K_global(dof_free,dof_free) \ rhs_global(dof_free);  
    du(dof_free) = du(dof_free) + delta_du;
      
    if norm(rhs_global(dof_free)) < err_tol
        converge = true;
        break
    end
end % Equilibrium iteration.

if converge
    % Report about completion:
    fprintf(['  Equilibrium was found in %d iterations with %d active slave ',...
        'nodes.\n'], iter, sum(active_nodes_w)) 
else
    error('Equilibrium was not found in %d iterations.\n', max_iter)
end

% Extract wheel and rail values:
du_w = du(1:n_dof_w);
du_r = du(n_dof_w+1:end);
rhs_global_w = rhs_global(1:n_dof_w);
rhs_global_r = rhs_global(n_dof_w+1:end);
stress_Mises_array_w = stress_Mises_array(1:n_el_w);
stress_Mises_array_r = stress_Mises_array(n_el_w+1:end);
end % function findEquilibrium.

