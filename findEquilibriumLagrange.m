function [ du_w, Lagrange, rhs_global_u_w, stress_Mises_array_w, ...
           du_r, rhs_global_u_r, stress_Mises_array_r ] = ...
         findEquilibriumLagrange( Edof_w, Ex_w, Ey_w, ...
                                  Edof_r, Ex_r, Ey_r, ...
                                  u_w, du_w, Lagrange, n_el_w, n_dof_u_w, dof_free_u_w, ...
                                  contact_nodes_coords_w, contact_dofs_w, active_nodes_w, ...
                                  u_r, du_r, n_el_r, n_dof_u_r, dof_free_u_r, ...
                                  contact_nodes_coords_r, contact_dofs_r, ...
                                  xi_l_array, xi_r_array, normal_array, ...
                                  mat_par, max_iter, err_tol, gap_tol )
%Function to represent FE equilibrium iteration.
%   Written to work for Lagrange multipliers method.
%
% Rostyslav Skrypnyk.

active_node_numbers_w = find(active_nodes_w);

% Create new arrays for the contact system, since 
% the system of equations cannot be decoupled:
Edof_r_renumbered = [n_el_w + Edof_r(:,1), n_dof_u_w + Edof_r(:,2:end)];
Edof = [Edof_w; Edof_r_renumbered]; % Convention: wheel 1st, rail 2nd.
Ex = [Ex_w; Ex_r];
Ey = [Ey_w; Ey_r];
u = [u_w; u_r]; 
du = [du_w; du_r];

n_el = n_el_w + n_el_r;
n_dof_u = n_dof_u_w + n_dof_u_r;
n_dof_free_u = length(dof_free_u_w) + length(dof_free_u_r);
n_extra_dof = sum(active_nodes_w); % Number of Lagrange multiplier DOFs.
n_total_dof = n_dof_u + n_extra_dof; % Total number of DOFs.

dof_extra = [n_dof_u + 1 : n_total_dof]';
dof_free_u_r_renumbered = n_dof_u_w + dof_free_u_r;
dof_free_u = [dof_free_u_w; dof_free_u_r_renumbered];
dof_free_total = [dof_free_u; dof_extra];
contact_dofs_r_renumbered = n_dof_u_w + contact_dofs_r;

stress_Mises_array = zeros(n_el,1); % To store von Mises stress in
% the elements.

converge = false; % Assume it does not converge.

for iter=1:max_iter
    u_el_array = extract(Edof,u);
    du_el_array = extract(Edof,du); % Extract nodal
    % increments of the displacement for each element.
    
    K_global = zeros(n_total_dof); % Global stiffness matrix.
    rhs_global = zeros(n_total_dof,1); % Global right hand side vector ==
    % internal forces - external forces.
    
    %% Assemble stiffness matrix and RHS vector
    for el=1:n_el
        [K_el,f_el, stresses_el] = elementRoutine(Ex(el,:), Ey(el,:),...
            mat_par, u_el_array(el,:)', du_el_array(el,:)');
        
        K_global(Edof(el,2:end),Edof(el,2:end)) = ...
            K_global(Edof(el,2:end),Edof(el,2:end)) + K_el;
        rhs_global(Edof(el,2:end)) = ...
            rhs_global(Edof(el,2:end)) + f_el;
        
        % For plotting:
        % Compute von Mises stress in the element:
        stresses_dev_el = stresses_el;
        stresses_dev_el(1:3) = stresses_el(1:3) - ...
            ones(3,1)*sum(stresses_el(1:3))/3.0;
        stress_Mises = sqrt(1.5* (stresses_dev_el'*stresses_dev_el) );
        
        stress_Mises_array(el) = stress_Mises;
    end % Loop over elements.
    %% Add contribution to the stiffness matrix and RHS vector from the contact nodes:
    contact_nodes_coords_w_new = contact_nodes_coords_w + ...
                                 u(contact_dofs_w) + ...
                                 du(contact_dofs_w);
    contact_nodes_coords_r_new = contact_nodes_coords_r +...
                                 u(contact_dofs_r_renumbered) +...
                                 du(contact_dofs_r_renumbered);
    % Get active slave nodes:
    [~, ~, ~, gap_func_array_all, ~] = contactSearch(contact_nodes_coords_w_new, ...
                                                     contact_nodes_coords_r_new, ...
                                                     gap_tol);
    gap_func_array = gap_func_array_all(active_nodes_w);
    if ~isempty( active_node_numbers_w )
        l_i = 0;
        for node_w=active_node_numbers_w' % transpose to iterate through the array.
            l_i = l_i + 1; % Lagrange DOF number.
            % Find the closest master node together with left and right nodes to it:
            [~,~,~,~,active_nodes_r] = contactSearch(contact_nodes_coords_w_new(node_w,:), ...
                                                     contact_nodes_coords_r_new, ...
                                                     gap_tol);
            node_r = find(active_nodes_r);
            [~, i_rail_sorted] = sortrows(contact_nodes_coords_r,1);
            [~,node_r_sorted] = min( abs(i_rail_sorted - node_r) );

            node_r_left = i_rail_sorted(node_r_sorted-1);
            node_r_right = i_rail_sorted(node_r_sorted+1);
            
            % Get nodal DOFs:
            node_active_dofs_w = contact_dofs_w(node_w,:);
            node_active_dofs_r = contact_dofs_r_renumbered(node_r,:);
            node_active_dofs_r_left = contact_dofs_r_renumbered(node_r_left,:);
            node_active_dofs_r_right = contact_dofs_r_renumbered(node_r_right,:);
            
            % Make simplification that normal, xi_left and xi_right are
            % constant during equilibrium iteration:
            N_vector = normal_array(node_w,:);
            xi_left = xi_l_array(node_w);
            xi_right = xi_r_array(node_w);
            
            % Derivatives for wheel:
            dGapFunc_dx_slave = N_vector;
            % and rail:
            dGapFunc_dx_closest = (xi_left + xi_right - 1) * N_vector;
            dGapFunc_dx_left = - xi_left * N_vector;
            dGapFunc_dx_right = - xi_right * N_vector;
            
            % Add contributions to the stiffness matrix:
            % RHS(slave_dof):
            K_global(node_active_dofs_w, dof_extra(l_i)) = ...
                K_global(node_active_dofs_w, dof_extra(l_i)) + dGapFunc_dx_slave';
            K_global(dof_extra(l_i), node_active_dofs_w) = ...
                K_global(dof_extra(l_i), node_active_dofs_w) + dGapFunc_dx_slave;
            % RHS(closest_dof):
            K_global(node_active_dofs_r, dof_extra(l_i)) = ...
                K_global(node_active_dofs_r, dof_extra(l_i)) + dGapFunc_dx_closest';
            K_global(dof_extra(l_i), node_active_dofs_r) = ...
                K_global(dof_extra(l_i), node_active_dofs_r) + dGapFunc_dx_closest;
            % RHS(left_from_closest_dof):
            K_global(node_active_dofs_r_left, dof_extra(l_i)) = ...
                K_global(node_active_dofs_r_left, dof_extra(l_i)) + dGapFunc_dx_left';
            K_global(dof_extra(l_i), node_active_dofs_r_left) = ...
                K_global(dof_extra(l_i), node_active_dofs_r_left) + dGapFunc_dx_left;
            % RHS(right_from_closest_dof):
            K_global(node_active_dofs_r_right, dof_extra(l_i)) = ...
                K_global(node_active_dofs_r_right, dof_extra(l_i)) + dGapFunc_dx_right';
            K_global(dof_extra(l_i), node_active_dofs_r_right) = ...
                K_global(dof_extra(l_i), node_active_dofs_r_right) + dGapFunc_dx_right;
            
            % Finally, add contribution to the residuals of the wheel:
            rhs_global(node_active_dofs_w) = rhs_global(node_active_dofs_w) + ...
                dGapFunc_dx_slave' * Lagrange(l_i);
            % and the rail closest:
            rhs_global(node_active_dofs_r) = rhs_global(node_active_dofs_r) + ...
                dGapFunc_dx_closest' * Lagrange(l_i);
            % rail left from closest:
            rhs_global(node_active_dofs_r_left) = rhs_global(node_active_dofs_r_left) + ...
                dGapFunc_dx_left' * Lagrange(l_i);
            % rail right from closest:
            rhs_global(node_active_dofs_r_right) = rhs_global(node_active_dofs_r_right) + ...
                dGapFunc_dx_right' * Lagrange(l_i);
        end % loop over active nodes.
    end % if there are active nodes.
    rhs_global(dof_extra) = gap_func_array;

    delta_dUnknowns =  - K_global(dof_free_total,dof_free_total) \ ...
        rhs_global(dof_free_total);
    
    du(dof_free_u) = du(dof_free_u) + delta_dUnknowns(1:n_dof_free_u);
    Lagrange = Lagrange + delta_dUnknowns(end-n_extra_dof+1:end);

    if norm(rhs_global(dof_free_u)) < err_tol && ...
       norm(rhs_global(dof_extra)) < gap_tol
        converge = true;
        break
    end
end % Equilibrium iteration.

if converge
    % Report about completion:
    fprintf(['  Equilibrium was found in %d iterations with %d active slave ',...
        'nodes.\n'], iter, sum(active_nodes_w)) 
else
    fprintf('  Equilibrium was not found in %d iterations.\n', max_iter)
end

% Extract wheel and rail values:
du_w = du(1:n_dof_u_w);
du_r = du(n_dof_u_w+1:end);
rhs_global_u_w = rhs_global(1:n_dof_u_w);
rhs_global_u_r = rhs_global(n_dof_u_w+1:n_dof_u);
stress_Mises_array_w = stress_Mises_array(1:n_el_w);
stress_Mises_array_r = stress_Mises_array(n_el_w+1:end);
end % function findEquilibriumLagrange.

