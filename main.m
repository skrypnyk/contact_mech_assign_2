% A script for the assignment 2 on the computational part of Contact
% Mechanics course.
%% Assignment 2: Two bodies.
% Based on CALFEM, solve contact problem between two elastic bodies 
% under plane strain conditions without friction.
%
% Rostyslav Skrypnyk. May, 2018.

%% Set-up
close all
clear variables
format compact
clc

addpath([pwd , '/meshes'])
addpath(genpath('~/Documents/MATLAB/calfem-3/')) % Add Calfem routines.

% Analysis settings:
mesh_coarse = false; % Choose coarse or dense mesh.
penalty_method = false; % Choose between penalty and Lagrange multipliers 
% method.

% Spatial discretization:
if mesh_coarse
    mesh_r = load('calfem_rail_coarse.mat');
    mesh_w = load('calfem_wheel_coarse.mat'); % 1x1 structure array
    % with the following fields (with description from CALFEM manual):
    % Ex, Ey -- arrays of rows [x_1 x_2 ... x_nen] and [y_1 y_2 ... y_nen], 
    %           where nen = number of element nodes and row i gives x  
    %           (or y) coordinates of the element defined in row i of Edof. 
    % Coord -- global nodal coordinate matrix; array of [x y] rows.
    % Edof -- element topology matrix; array of [el dof_1 dof_2 ... dof_ned] 
    %         rows; el = element number, ned = number of element dof.
    % Dof -- global topology matrix; array of [k l .. m] rows. The nodal 
    %        coordinates defined in row i of Coord correspond to the 
    %        degrees  of freedom of row i in Dof.
    % dof_free
    % dof_prescr
    % contact_nodes.
    fprintf('Coarse mesh was chosen.\n\n');
else
    mesh_r = load('calfem_rail_dense.mat');
    mesh_w = load('calfem_wheel_dense.mat');
    
    fprintf('Dense mesh was chosen.\n\n');
end

if penalty_method
    fprintf('Penalty method was chosen.\n\n');
else
    fprintf('Lagrange multipliers method was chosen.\n\n');
end

% Loading:
u_vert = - 0.15 % Vertical displacement of the top of the wheel [mm].
u_horiz = 0.;

% Time stepping:
n_steps_vert = 10;
n_steps_horiz = 0;
n_steps = n_steps_vert + n_steps_horiz;
t_end_vert = 0.05; % [s].
t_end_horiz = 0.0;
t_end_total = t_end_vert + t_end_horiz;
dt = t_end_vert/n_steps_vert;

% Material parameters:
mat_par.E = 200.e3; % Young's modulus [MPa=N/mm^2].
mat_par.nu = 0.3; % Poisson's ratio [unitless].
mat_par.penalty = 1.e7; % Penalty parameter [N/mm].
mat_par.friction = 0.3; % Friction coefficient [unitless].

eps_0 = 1.e-2; % Regularization variable [mm/s].

% Tolerances:
err_tol = 1.e-6; % Error tolerance for the Newton's iteration.
gap_tol = 1.e-15; % Tolerance for the gap function to decide on active nodes.
max_iter = 50;

% Other quantities:
n_dof_w = max(max(mesh_w.Dof)); % Number of degrees of freedom.
n_dof_r = max(max(mesh_r.Dof));

n_dof_el_w = size(mesh_w.Edof,2) - 1; % Number of DOFs per element.
n_dof_el_r = size(mesh_r.Edof,2) - 1;

n_el_w = size(mesh_w.Edof,1); % Number of elements.
n_el_r = size(mesh_r.Edof,1);

dof_free_w = mesh_w.dof_free;
dof_free_r = mesh_r.dof_free;

dof_prescr_horiz_w = mesh_w.dof_prescr(1:2:end);
dof_prescr_horiz_r = mesh_r.dof_prescr(1:2:end);

dof_prescr_vert_w = mesh_w.dof_prescr(2:2:end);
dof_prescr_vert_r = mesh_r.dof_prescr(2:2:end);

contact_dofs_w = mesh_w.Dof(mesh_w.contact_nodes,:); % DOFs of the nodes on 
% the contact surface.
contact_dofs_r = mesh_r.Dof(mesh_r.contact_nodes,:);

contact_nodes_coords_w = mesh_w.Coord(mesh_w.contact_nodes,:);
contact_nodes_coords_r = mesh_r.Coord(mesh_r.contact_nodes,:);

% Initialize:
dof_prescr_w = dof_prescr_vert_w;
u_w = zeros(n_dof_w,1); % Displacements of the wheel at time t+dt.
u_r = zeros(n_dof_r,1);

[xi_l_array, xi_r_array, normal_array, gap_func_array, ...
 active_nodes_r] = contactSearch(contact_nodes_coords_w, ...
                                 contact_nodes_coords_r, ...
                                 gap_tol);

Lagrange = zeros(sum(active_nodes_r), 1); % Check how many nodes are in the immediate contact and initialize with zero.

u_hist_w = [];
u_hist_r = [];

forces_hist_w = [];
forces_hist_r = [];

stress_Mises_hist_w = [];
stress_Mises_hist_r = [];

%% Solution
time = dt;
du_w = zeros(n_dof_w,1);
du_r = zeros(n_dof_r,1);
while time <= t_end_total
    if time <= t_end_vert 
        du_prescr_w = u_vert/n_steps_vert*ones(size(dof_prescr_w)); % Increment of the 
        % prescribed vertical displacement.
        
        % Compute current nodal position:
        contact_nodes_coords_w_new = contact_nodes_coords_w + u_w(contact_dofs_w);
        contact_nodes_coords_r_new = contact_nodes_coords_r + u_r(contact_dofs_r);
        % Compute gap function and active master nodes assuming increment 
        % in the displacements from the previous time step:
        [xi_l_array, xi_r_array, normal_array, gap_func_array, ...
         ~] = contactSearch(contact_nodes_coords_w_new, ...
                            contact_nodes_coords_r_new, ...
                            gap_tol);
        active_nodes_w = getActiveNodes(gap_func_array, gap_tol);
    else % Prescribe horizontal DOFs:
        fprintf('Sliding.\n\n');

        dof_prescr_w = dof_prescr_horiz_w;
        du_prescr_w = u_horiz/n_steps_horiz*ones(size(dof_prescr_w));
        dt = t_end_horiz/n_steps_horiz;
        
        % Fix vertical position:
        du_w(dof_prescr_vert_w) = zeros(size(dof_prescr_vert_w));
    end
    
    % Prescribe BCs:
    du_w(dof_prescr_w) = du_prescr_w;

    for iter=1:max_iter % Contact iteration:
        fprintf(' Start contact iteration %d.\n', iter)
        if penalty_method
            % Solve for equilibrium and update increment of the displacements:
            [du_w, forces_w, stress_Mises_array_w, ...
             du_r, forces_r, stress_Mises_array_r] = findEquilibrium(...
                mesh_w.Edof, mesh_w.Ex, mesh_w.Ey, ...
                mesh_r.Edof, mesh_r.Ex, mesh_r.Ey, ...
                u_w, du_w, n_el_w, n_dof_w, dof_free_w, ...
                contact_nodes_coords_w, contact_dofs_w, ...
                active_nodes_w, ...
                u_r, du_r, n_el_r, n_dof_r, dof_free_r, ...
                contact_nodes_coords_r, contact_dofs_r, ...
                xi_l_array, xi_r_array, normal_array, ...
                mat_par, max_iter, err_tol, gap_tol);
            
            % Recompute gap function:
            contact_nodes_coords_w_new = contact_nodes_coords_w + u_w(contact_dofs_w) + du_w(contact_dofs_w);
            contact_nodes_coords_r_new = contact_nodes_coords_r + u_r(contact_dofs_r) + du_r(contact_dofs_r);
            [xi_l_array, xi_r_array, normal_array, gap_func_array, ...
             ~] = contactSearch(contact_nodes_coords_w_new, ...
                                contact_nodes_coords_r_new, ...
                                gap_tol);

            % Find the active nodes after the equilibrium has been found:
            active_nodes_w_new = getActiveNodes(gap_func_array, gap_tol);
          
        else % Lagrange multiplier method:
            active_sum = sum(active_nodes_w);
            if isempty(Lagrange)
                Lagrange = zeros(active_sum,1);
            elseif active_sum ~= length(Lagrange)
                L_w_temp = Lagrange(Lagrange < 0); % Positive values mean nodes are not in contact.
                Lagrange = zeros(active_sum,1);
                active_ind_old_w = find(active_nodes_w_old); % Indices of active nodes from end of previous iteration.
                active_ind_w = find(active_nodes_w);
                [~,~,active_ind_both_w] = intersect(active_ind_old_w, active_ind_w);
                Lagrange(active_ind_both_w) = L_w_temp;
            end
            % Solve for equilibrium and update increment of the displacements:
            [du_w, Lagrange, forces_w, stress_Mises_array_w,...
             du_r, forces_r, stress_Mises_array_r] = findEquilibriumLagrange(...
                mesh_w.Edof, mesh_w.Ex, mesh_w.Ey, ...
                mesh_r.Edof, mesh_r.Ex, mesh_r.Ey, ...
                u_w, du_w, Lagrange, n_el_w, n_dof_w, dof_free_w, ...
                contact_nodes_coords_w, contact_dofs_w, ...
                active_nodes_w, ...
                u_r, du_r, n_el_r, n_dof_r, dof_free_r, ...
                contact_nodes_coords_r, contact_dofs_r, ...
                xi_l_array, xi_r_array, normal_array, ...
                mat_par, max_iter, err_tol, gap_tol);

            % Recompute gap function:
            contact_nodes_coords_w_new = contact_nodes_coords_w + u_w(contact_dofs_w) + du_w(contact_dofs_w);
            contact_nodes_coords_r_new = contact_nodes_coords_r + u_r(contact_dofs_r) + du_r(contact_dofs_r);
            [xi_l_array, xi_r_array, normal_array, gap_func_array, ...
             ~] = contactSearch(contact_nodes_coords_w_new, ...
                                contact_nodes_coords_r_new, ...
                                gap_tol);
                            
            % Find the active nodes after the equilibrium has been found:
            active_nodes_w_new = getActiveNodesLagrange(gap_func_array, gap_tol, Lagrange);
        end
        
        if all(active_nodes_w==active_nodes_w_new)
            % If the set of active contact nodes has not changed:
            break
        end
        active_nodes_w_old = active_nodes_w;
        active_nodes_w = active_nodes_w_new;
    end % Contact iteration.
    
    if iter==max_iter
        error('Contact iteration did not converge in %d iterations.\n', ...
            max_iter)
    else
        fprintf('Competed %.1f %%.\n\n', time / t_end_total * 100 )
    end
    u_w = u_w + du_w;
    u_r = u_r + du_r;
    u_hist_w = [u_hist_w, u_w];
    u_hist_r = [u_hist_r, u_r];
    forces_hist_w = [forces_hist_w, forces_w];
    forces_hist_r = [forces_hist_r, forces_r];
    stress_Mises_hist_w = [stress_Mises_hist_w, stress_Mises_array_w];
    stress_Mises_hist_r = [stress_Mises_hist_r, stress_Mises_array_r];
    time = time + dt;
end % Loop over time.

%% Post-process
% Save
% max_penetration = abs(min(gap_func_array(gap_func_array<0)));
% save('penalty1e7.mat','u_hist_w','dof_prescr_vert_w','u_vert', ...
%      'forces_hist_w','max_penetration');
% save('lagrange.mat','u_hist_w','dof_prescr_vert_w','u_vert', ...
%      'forces_hist_w');

% Plots:
figure(1)
% Plot undeformed shape:
eldraw2(mesh_w.Ex, mesh_w.Ey,[2 1 0]) % Black.
eldraw2(mesh_r.Ex, mesh_r.Ey,[2 4 0]) % Red.

% Plot deformed shape:
u_el_array_w = extract(mesh_w.Edof, u_w);
u_el_array_r = extract(mesh_r.Edof, u_r);
eldisp2(mesh_w.Ex,mesh_w.Ey,u_el_array_w,[1,2,0],1);
eldisp2(mesh_r.Ex,mesh_r.Ey,u_el_array_r,[1,4,0],1);
%xlim([-36,36])
%ylim([-40,41])

% Task a:
% plotstask1

% Task b:
plotstask2