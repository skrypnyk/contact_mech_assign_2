function [ xi_l_array, xi_r_array, normal_array, ...
           gap_func, active_master ] = contactSearch(slave_coords, ...
                                                     master_coords, tol)
% Performs node to segment contact search and returns xi_l, xi_r, normal, 
% gap function and active nodes among master (rail) nodes.
%
% Input:
% slave_coords -- coordinates of slave (wheel) nodes.
% master coords -- coordinates of master (rail) nodes.
%
% Output:
% xi_l_array -- coordinate along master surface on the segment to the LEFT
%               from the closest master node, where normal points to slave
%               node.
% xi_r_array -- same as x_l, but on the segment to the RIGHT.
% normal_array -- vector, normal to the segment on the master surface with
%                 a positive xi coordinate.
% gap_func -- values of gap function.
% active_master -- boolean array to represent which master nodes are 
%                  active.
%
% /Rostyslav Skrypnyk.

N_slaves = size(slave_coords,1);
N_masters = size(master_coords,1);

[slave_coords_sorted, i_slave_sorted ]= sortrows(slave_coords,1);
[master_coords_sorted, i_master_sorted] = sortrows(master_coords,1);

xi_l_array = zeros(N_slaves,1); % There are as many coordinates on 
                                % the master surface, as there are slave
                                % nodes.
xi_r_array = xi_l_array;

normal_array = zeros(N_slaves,2);

gap_func = zeros(N_slaves,1);

active_master = zeros(N_masters,1);

for n=1:N_slaves % for each slave node:
    %% Identify the closest master node and its neighbours:
    [~,closest_master] = min( abs(master_coords_sorted(:,1) - slave_coords_sorted(n,1)) );

    left_master = closest_master - 1;
    right_master = closest_master + 1;
   
    if left_master == 0 % if closest is the leftmost node:
        left_master = closest_master;
    elseif right_master == N_masters + 1 % if closest is the rightmost node:
        right_master = closest_master;
    end
    %% Identify closest master point (xi_l and xi_r) and the normal:
    if left_master == closest_master
        xi_l = 0;
    else
        xi_l = (slave_coords_sorted(n,1) - master_coords_sorted(closest_master,1)) / ...
               (master_coords_sorted(left_master,1) - master_coords_sorted(closest_master,1));
    end
    
    if right_master == closest_master
        xi_r = 0;
    else 
        xi_r = (slave_coords_sorted(n,1) - master_coords_sorted(closest_master,1)) / ...
               (master_coords_sorted(right_master,1) - master_coords_sorted(closest_master,1));
    end
    
    diff = master_coords_sorted(closest_master,:) - master_coords_sorted(left_master,:);
    v = diff / norm(diff+eps);
    left_normal = cross([0,0,1],[v,0]);
    
    diff = master_coords_sorted(right_master,:) - master_coords_sorted(closest_master,:);
    v = diff / norm(diff+eps);
    right_normal = cross([0,0,1],[v,0]);
    
    if xi_l <= 0 % if normal to slave node connects 
        % on the right from the closest master node:
        xi_l = 0;
        if norm(right_normal) > 0 % if not edge segment:
            normal = right_normal;
        end
    end
    if xi_r <= 0 % if normal to slave node connects 
        % on the left from the closest master node:
        xi_r = 0;
        if xi_l <= 0 % if both are negative, choose average normal:
            if norm(left_normal) > 0 && norm(right_normal) > 0 % if not an edge case:
                normal = 0.5 * (left_normal + right_normal);
            end
        elseif norm(left_normal) > 0
            normal = left_normal;
        end
    end
    
    if xi_l > 0 && xi_r > 0 % if v-shaped segment formation:
        % choose segment, closest to the slave node:
        dist_l = norm( master_coords_sorted(closest_master,:) + ...
                       xi_l * (master_coords_sorted(left_master,:) - ...
                               master_coords_sorted(closest_master,:)) ...
                      );
        dist_r = norm( master_coords_sorted(closest_master,:) + ...
                       xi_r * (master_coords_sorted(right_master,:) - ...
                               master_coords_sorted(closest_master,:)) ...
                      );
        if dist_l < dist_r
            normal = left_normal;
        else
            normal = right_normal;
        end
    end % usual case:
    closest_master_point = master_coords_sorted(closest_master,:) + ...
        xi_l * (master_coords_sorted(left_master,:) - ...
        master_coords_sorted(closest_master,:)) + ...
        xi_r * (master_coords_sorted(right_master,:) - ...
        master_coords_sorted(closest_master,:));
    %% Compute gap function:
    gap_func(n) = dot(slave_coords_sorted(n,:) - closest_master_point,normal(1:2));
    %% Identify active master nodes:
    if gap_func(n) < tol || N_slaves == 1 % There's 1 slave node passes when it is active.
        active_master(closest_master) = 1; % mark as active.
    end
    %% Store nodal values:
    xi_l_array(n) = xi_l;
    xi_r_array(n) = xi_r;
    normal_array(n,:) = normal(1:2);
end % loop over slave nodes.

% Convert back the order:
xi_l_array(i_slave_sorted) = xi_l_array;
xi_r_array(i_slave_sorted) = xi_r_array;
normal_array(i_slave_sorted,:) = normal_array;

gap_func(i_slave_sorted) = gap_func;
active_master(i_master_sorted) = active_master;

end % function.

