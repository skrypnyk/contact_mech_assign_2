[//]: # (To preview markdown file in Emacs type C-c C-c p)

# Computer assignment 2: Two bodies contact
MATLAB code to solve contact between two elastic bodies under
plane strain conditions without friction.

## Task A
Node to segment gap function.

## Task B
Penalty method.

## Task C
Lagrange multiplier method.

